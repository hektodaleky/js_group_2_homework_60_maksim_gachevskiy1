import React, {Component, Fragment} from "react";
import InputForm from "../components/InputForm";
import OnePost from "../components/OnePost/OnePost";
import StupidButton from "../components/UI/StupidButton";
class Chat extends Component {
    state = {
        myName: "",
        myPost: "",
        postsArray: [],
        chatWindow: true
    };

    deletePosts = () => {

        this.setState(prevState => {
            console.log('[Blog] Toggling form');
            return ({chatWindow: !prevState.chatWindow}
        );});
        clearInterval(this.interval)
};


changeName = event => this.setState({myName: event.target.value});

    changePost = event => this.setState({myPost: event.target.value});

    sendQuestion = url => {
        fetch(url).then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong with network request');

        }).then(posts => {


            this.setState({postsArray: posts});
        }).catch(error => {
            console.log(error);
        });
        console.log("Hey")
    };


    componentDidMount=()=> {
        this.interval = setInterval(() => this.sendQuestion('http://146.185.154.90:8000/messages'), 3000);

    };
    componentWillUnmount=()=>{
        clearInterval(this.interval);
        console.log("Unmount");
        //Не отрабатывает в таком виде...
    };
    

    addPost = () => {
        const name = this.state.myName;
        const text = this.state.myPost;
        const post = new URLSearchParams();
        post.append('author', name);
        post.append('message', text);
        const config = {
            method: 'POST',
            body: post
        };

        fetch("http://146.185.154.90:8000/messages?datetime", config);
        this.setState({
            myPost: ""
        })

    };


    render() {

        return (<Fragment>

                {(this.state.chatWindow) ? <div className="chatWindow">
                    {this.state.postsArray.map((post, INDEX) => {
                        return <OnePost author={post.author} post={post.message} time={post.datetime}
                                        key={post._id}/>
                    })}</div> : null}
                <InputForm changeAuthor={(event => this.changeName(event))}
                           changeText={(event => this.changePost(event))} clickButton={this.addPost}
                           authorValue={this.state.myName} postValue={this.state.myPost}/>
                <StupidButton click={this.deletePosts}/>
            </Fragment>
        );
    }
}
;
export default Chat;